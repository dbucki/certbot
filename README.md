
# Usage

## Set env variables

```
S3_BUCKET
AWS_ACCESS_KEY_ID
AWS_SECRET_ACCESS_KEY
LOADBALANCER_USERNAME
LOADBALANCER_PASSWORD
```

## Run script

```sh
$ git clone $(repo-url)
$ cd ./certbot
$ ./certbot.sh -d example.com -d example2.com -d sub.example.com
```

