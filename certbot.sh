#!/bin/bash

# instalacja certbota
if [ ! -f certbot-auto ]; then
    wget https://dl.eff.org/certbot-auto
    chmod a+x ./certbot-auto
fi

# instalacja nvm
if [ ! command -v nvm &>/dev/null ]; then
    wget -qO- https://raw.githubusercontent.com/creationix/nvm/v0.33.11/install.sh | bash

    export NVM_DIR="$HOME/.nvm"
    [ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh" # This loads nvm

    nvm install --lts
fi

# upgrade pip
sudo pip install --upgrade pip

# uruchomienie certbota
./certbot-auto certonly --manual --manual-auth-hook ./hooks/auth.sh --manual-cleanup-hook ./hooks/cleanup.sh $@

# kopia certyfikatow
sudo cp -r /etc/letsencrypt/archive/ ~/letsencrypt/
sudo chmod -R 644 ~/letsencrypt/