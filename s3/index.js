const AWS = require('aws-sdk');

const commandMap = {
    upload: 'putObject',
    delete: 'deleteObject'
};

const accessKeyId = process.env.AWS_ACCESS_KEY_ID;
const secretAccessKey = process.env.AWS_SECRET_ACCESS_KEY;
const bucket = process.env.S3_BUCKET;

if (typeof accessKeyId !== 'string' || typeof secretAccessKey !== 'string' || typeof bucket !== 'string') {
    throw new Error('Bad s3 credentials');
}

const s3 = new AWS.S3({
    region: 'eu-central-1',
    s3ForcePathStyle: true,
    sslEnabled: true,
    accessKeyId: accessKeyId,
    secretAccessKey: secretAccessKey
});

const args = process.argv.slice(2);
const command = args.shift();

if (typeof commandMap[command] === 'undefined') {
    throw new Error('Bad s3 command');
}

const s3Command = commandMap[command].toString();

const s3Params = {
    Bucket: bucket,
    Key: `certbot/${process.env.CERTBOT_DOMAIN}/${process.env.CERTBOT_TOKEN}`
};

if (s3Command === commandMap.upload) {
    s3Params.Body = process.env.CERTBOT_VALIDATION;
    s3Params.ContentType = 'text/plain;charset=utf-8';
}

s3[s3Command](s3Params, (err) => {
    if (err) {
        throw err;
    }

    console.log(`Successfully ${command} certbot validation /${s3Params.Bucket}/${s3Params.Key}`);
});