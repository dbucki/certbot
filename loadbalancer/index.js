const async = require('async');
const request = require('request');

const vhost = process.env.CERTBOT_DOMAIN;
const bucket = process.env.S3_BUCKET;

if (typeof process.env.LOADBALANCER_USERNAME !== 'string' || typeof process.env.LOADBALANCER_PASSWORD !== 'string' || typeof bucket !== 'string') {
    throw new Error('Bad loadbalancer credentials');
}

const loadbalancerAuth = {
    username: process.env.LOADBALANCER_USERNAME,
    password: process.env.LOADBALANCER_PASSWORD
};
const loadbalancerApi = {
    host: '', // endpoint loadbalancera
    version: '' // api ver
};

const args = process.argv.slice(2);
const command = args.shift();

if (command !== 'addProxy' && command !== 'deleteProxy') {
    throw new Error('Bad loadbalancer command');
}

const getLoadbalancerApiUrl = (path) => {
    return `http://${loadbalancerApi.host}/${loadbalancerApi.version}/${path}`;
};

async.auto({
    getProxyConfig: (callback) => {
        const config = {
            source: '^/.well-known/acme-challenge/(.+)$',
            target: {
                proxyReplace: `/${bucket}/certbot/${vhost}/$1`,
                proxyHost: `${bucket}.s3-eu-central-1.amazonaws.com`,
                proxyPort: 80,
                protocol: 'http'
            }
        };

        return async.setImmediate(() => {
            return callback(null, config);
        });
    },
    getHost: (callback) => {
        request({
            url: getLoadbalancerApiUrl(`hosts/${vhost}`),
            auth: loadbalancerAuth,
            method: 'GET',
            json: true
        }, (err, response, body) => {
            return callback(err, {
                config: body,
                variants: body.traffic.map((item) => {
                    return item.variant
                })
            });
        });
    },
    getVariants: ['getHost', (results, callback) => {
        async.map(results.getHost.variants, (variant, callback) => {
            request({
                url: getLoadbalancerApiUrl(`hosts/${vhost}/variants/${variant}`),
                auth: loadbalancerAuth,
                method: 'GET',
                json: true
            }, (err, response, body) => {
                return callback(err, {
                    name: variant,
                    config: body,
                    version: response.headers.etag
                });
            });
        }, callback);
    }],
    prepareNewConfigs: ['getProxyConfig', 'getVariants', (results, callback) => {
        async.map(results.getVariants, (variant, callback) => {
            const config = Object.assign({}, variant.config);

            if (command === 'addProxy') {
                if (Array.isArray(config.proxies)) {
                    config.proxies.unshift(results.getProxyConfig);
                } else {
                    config.proxies = [results.getProxyConfig];
                }
            } else if (command === 'deleteProxy') {
                if (Array.isArray(config.proxies)) {
                    for (let i = config.proxies.length - 1; i >= 0; i--) {
                        const proxyConfig = config.proxies[i];

                        if (
                            (proxyConfig.source === results.getProxyConfig.source) &&
                            (proxyConfig.target.proxyReplace === results.getProxyConfig.target.proxyReplace)
                        ) {
                            config.proxies.splice(i, 1);
                        }
                    }

                    if (config.proxies.length === 0) {
                        delete config.proxies;
                    }
                }
            }

            return async.setImmediate(() => {
                return callback(null, {
                    name: variant.name,
                    config: config,
                    version: variant.version
                });
            })
        }, callback);
    }],
    updateVariants: ['prepareNewConfigs', (results, callback) => {
        async.map(results.prepareNewConfigs, (variant, callback) => {
            request({
                url: getLoadbalancerApiUrl(`hosts/${vhost}/variants/${variant.name}`),
                auth: loadbalancerAuth,
                method: 'PUT',
                json: true,
                headers: {
                    'If-Match': variant.version
                },
                body: variant.config
            }, (err) => {
                return callback(err);
            });
        }, callback);
    }],
    checkDeployStatus: ['getHost', 'getVariants', 'updateVariants', (results, callback) => {
        if (command === 'deleteProxy') {
            return async.setImmediate(() => {
                return callback();
            });
        }

        const variantNames = Object.assign([], results.getHost.variants);
        const checkInterval = 15 * 1000; // 15s

        async.doWhilst((callback) => {
            async.each(variantNames, (variant, callback) => {
                request({
                    url: `http://${host}/.well-known/acme-challenge/${process.env.CERTBOT_TOKEN}`
                }, (err, response, body) => {
                    if (err) {
                        return callback(err);
                    }

                    if (response.statusCode === 200 && body === process.env.CERTBOT_VALIDATION) {
                        variantNames.splice(variantNames.indexOf(variant), 1);
                    }

                    return callback();
                });
            }, () => {
                setTimeout(() => {
                    return callback();
                }, checkInterval);
            });
        }, () => {
            return variantNames.length;
        }, callback);
    }]
}, (err, results) => {
    if (err) {
        throw err;
    }

    console.log(`Successfully loadbalancer ${command} deployed to host: ${vhost}`);
});