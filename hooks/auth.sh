#!/bin/bash

# node lts
nvm use --lts

# export 
export CERTBOT_DOMAIN=$CERTBOT_DOMAIN
export CERTBOT_VALIDATION=$CERTBOT_VALIDATION
export CERTBOT_TOKEN=$CERTBOT_TOKEN
export CERTBOT_CERT_PATH=$CERTBOT_CERT_PATH
export CERTBOT_KEY_PATH=$CERTBOT_KEY_PATH
export CERTBOT_SNI_DOMAIN=$CERTBOT_SNI_DOMAIN

cd ./s3
if [ ! -d node_modules ]; then
    npm install -s
fi
npm start upload


cd ./../loadbalancer
if [ ! -d node_modules ]; then
    npm install -s
fi
npm start addProxy